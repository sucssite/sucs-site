<div class="box">
<div class="boxhead">
<h2>Tom Lake (tswsl1989) - "Quantum Computation and Communication"</h2>
</div>
<div class="boxcontent">
<p>A brief introduction to Quantum computing theory and some of the possible practical implementations</p>
<!--
<div id="player">
<object
data="http://sucs.org/videos/talks/mediaplayer.swf?file=2008-10-16/worldinsideme.flv"
height="275" id="player" type="application/x-shockwave-flash" width="320">
<param name="height" value="256" />
<param name="width" value="320" />
<param name="file" value="/videos/talks/2008-10-16/worldinsideme.flv" />
<param name="image" value="/videos/talks/2008-10-16/worldinsideme.png" />
<param name="id" value="player" />
<param name="displayheight" value="256" />
<param name="FlashVars"
value="image=/videos/talks/2008-10-16/worldinsideme.png" />
</object>
</div>
<p><b>Length: </b>12m 45s</p>
<p><b>Video: </b><a href="http://sucs.org/videos/talks/2011-11-01/itsme.ogv" mce_href="http://sucs.org/videos/talks/2011-11-01/itsme.ogv" title="720x576 Ogg
Theora - 20MB">720x576</a> (Ogg Theora, 20MB)</p>
-->
<p>A video of this presentation was not recorded :-(</p>
<p>tswsl1989's slides are available <a href="/videos/talks/2012-02-16/tswsl1989.pdf">here</a>.</p>
</div>
<div class="boxfoot">
<p>&nbsp;</p>
</div>
</div>
