<div class="box">
	<div class="boxhead"><h2>Chris Jones (rollercow) - Hiking + Geek</h2></div>
	<div class="boxcontent">
	<p>Chris Jones tells us about how technology combines with his love of hiking, through gadgets, photos, and mashups.</p>

<div id="player"><object type="application/x-shockwave-flash" data="/videos/talks/mediaplayer.swf?file=2007-05-15/2007-05-15-rollercow.flv" width="320" height="260" allowscriptaccess="always" allowfullscreen="true" file="/videos/talks/2007-05-15/2007-05-15-rollercow.flv" image="/videos/talks/2007-05-15/2007-05-15-rollercow.png" id="player" displayheight="240"><param name="height" value="240" /><param name="width" value="320" />
<param name="file" value="/videos/talks/2007-05-15/2007-05-15-rollercow.flv" />
<param name="image" value="/videos/talks/2007-05-15/2007-05-15-rollercow.png" />
<param name="id" value="player" />
<param name="displayheight" value="240" />
<param name="FlashVars" value="image=/videos/talks/2007-05-15/2007-05-15-rollercow.png" />
</object></div>

<p><strong>Length: </strong>24m 19s</p>
<p><strong>Video:</strong><a href="/videos/talks/2007-05-15/2007-05-15-rollercow.mov" title="784x576 H.264 .mov - 191.4MB">784x576</a>(H.264 .mov, 191.4MB98.3MB)</p>
<p><strong>Slides:</strong>Coming Soon (PDF, -)<br /></p>
</div>
	<div class="boxfoot"><p>&nbsp;</p></div>
</div>
