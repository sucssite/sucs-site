<div class="box">
	<div class="boxhead"><h2>Andy Price (welshbyte) - FLOSSing Your Bits</h2></div>
	<div class="boxcontent">
	<p>Andy Price (maintainer of <a href="http://andrewprice.me.uk/projects/pybackpack/" title="Pybackpack">Pybackpack</a>) shares some tips on how to start a free and open source project, and how to get involved with the open source community.</p>

<div id="player"><object type="application/x-shockwave-flash" data="/videos/talks/mediaplayer.swf?file=2007-05-15/2007-05-15-welshbyte.flv" width="320" height="260" allowscriptaccess="always" allowfullscreen="true" file="/videos/talks/2007-05-15/2007-05-15-welshbyte.flv" image="/videos/talks/2007-05-15/2007-05-15-welshbyte.png" id="player" displayheight="240"><param name="height" value="240" /><param name="width" value="320" />
<param name="file" value="/videos/talks/2007-05-15/2007-05-15-welshbyte.flv" />
<param name="image" value="/videos/talks/2007-05-15/2007-05-15-welshbyte.png" />
<param name="id" value="player" />
<param name="displayheight" value="240" />
<param name="FlashVars" value="image=/videos/talks/2007-05-15/2007-05-15-welshbyte.png" />
</object></div>

<p><strong>Length: </strong>16m 14s</p>
<p><strong>Video:</strong><a href="/videos/talks/2007-05-15/2007-05-15-welshbyte.mov" title="784x576 H.264 .mov - 98.3MB">784x576</a>(H.264 .mov, 98.3MB)</p>
<p><strong>Slides:</strong> <a href="/videos/talks/2007-05-15/2007-05-15-welshbyte-slides.pdf" title="FLOSSing Your Bits">2007-05-15-welshbyte-slides.pdf</a> (PDF, 120KB)<br /></p>
</div>
	<div class="boxfoot"><p>&nbsp;</p></div>
</div>
