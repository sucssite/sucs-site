<div class="box">
<div class="boxhead">
<h2><a id="20">LAN Gaming @ Swansea Uni</a></h2>
</div>
<div class="boxcontent">
<p>No LAN events are currently planned. Pester your friendly SUCS executive committee to organise one!</p>

<!--<p>The full list of games that may make an appearance can be found <a href="http://sucs.org/Community/Forum/viewtopic.php?id=183">here</a>.</p>
<p>The full list of things to bring can be seen <a href="http://sucs.org/Community/Forum/viewtopic.php?id=184">here</a>.</p>
-->
</div>
<div class="boxfoot">
<p>&nbsp;</p>
</div>
</div>
<h2>Frequently Asked Questions</h2>
<h3>What is a LAN party?</h3>
<p>At a LAN party, people bring their computers along, network them together, and play multiplayer games against each other. It's great fun, and as well as letting you get your game on, it's also a great opportunity to socialise with people with similar interests.</p>
<p align="center"><a href="../pictures/lan1.jpg" title="Our first LAN event!"><img alt="Our first LAN event" src="../pictures/lan1_thumb.jpg" /></a>&nbsp;&nbsp;&nbsp;<a href="../pictures/lan2.jpg" title="Our first LAN event!"><img alt="Our first LAN event" src="../pictures/lan2_thumb.jpg" /></a>&nbsp;&nbsp;&nbsp;<a href="../pictures/lan3.jpg" title="Our first LAN event!"><img alt="Our first LAN event" src="../pictures/lan3_thumb.jpg" /></a></p>
<h3>What games will we play?</h3>
<p>It's mostly up to you!&nbsp; However we'll try to organise a structured tournament for CounterStrike:Source as well as Unreal Tournament if enough people show up with those games installed. </p>
<p>In any case, we'll be running servers of games like Counter Strike: Source, Half-Life 2 Deathmatch, and Unreal Tournament to name a few. We'll have a wide range of games, including some that are entirely free to download - so don't worry if your PC's not quite up to running the latest and greatest games. And let us know if you have any requests! Just because we'll be running some servers, that doesn't stop you running your own on the network, too.&nbsp; Also just give us an email at games [at] sucs [dot] org to nominate any extra games you'd like to see. </p>
<p>&nbsp;An up to date list of games that&nbsp; may be played can be found <a href="http://sucs.org/Community/Forum/viewtopic.php?id=183">on the forum</a></p>
<h3>Who can come?</h3>
<p>Anyone's welcome to come to the LAN Event as long as you're a student at Swansea University. Please <strong>make sure you bring your student card</strong>, otherwise we won't be able to let you in. &nbsp;</p>
<h3>What do I need to bring?</h3>
<ul>
<li>Your PC - the base unit, monitor, mouse, keyboard, mousemat, headphones, etc.
<ul>
<li>Please note <strong>speakers are NOT allowed</strong> at the event - it's far too noisy with a room full of people all with their own speakers. It also uses less power, too! <br /></li>
<li>Please ensure you only bring <strong>one</strong> base unit, too, unless you arrange it with us specially.</li>
<li>Make sure you bring all the relevant cables you need - power, monitor, etc. </li>
<li>Also see <a href="#config" title="How do I need to configure my system?">How do I need to configure my system?</a> </li>
</ul>
</li>
<li>A power strip with enough sockets for what you need to plug in (probably just your monitor and base unit) plus any external drives you want to bring.<br /></li>
<li>CDs for all the games you'll want to be playing. It may also be a good idea to bring your Operating System installation CD, in case you need to reinstall anything.</li>
<li>Food and (non-alcoholic) drink to keep you going. See <a href="#fooddrink" title="Can I bring food and drink?">Can I bring food and drink?</a></li>
<li>Of course, your computer will also need to be fitted with a standard RJ45 LAN socket - either inbuilt, USB Driven or on a PCI card.</li>
<li>Payment for the LAN.&nbsp; If you pre-pay at the SUCS room before they day to one of the SUCS commitee it will only be &pound;1, otherwise it is &pound;2 on the day.&nbsp; We only have a select level of spaces so please email games[at]sucs[dot]org before the day to indicate your intention to come.&nbsp; </li>
</ul>
<h3>How do I get my stuff there?</h3>
<p>If you're on campus, then obviously you can just bring it by hand. Maybe get a friend to help, so you can get it moved faster. If you're off campus, the easiest method is probably to get a taxi. A taxi from the
  student village should only cost around &pound;3 and from Mumbles no more than &pound;5. </p>
<h3>Can I bring food and drink?<a name="fooddrink" title="fooddrink"></a></h3>
<p>You certainly can, and it's probably best to. Just bring enough snackfood, fizzy drinks, etc to last you through the day. Please note, however, that <strong>you are NOT allowed to bring alcohol</strong>, as the venue is licensed. There is, however, a bar in the building. </p>
<h3>How do I need to configure my system?<a name="config" title="config"></a></h3>
<p>Please ensure that all your games are <strong>fully up to date</strong> with the latest patches (including your Steam games!) and that you allow Steam to run in <strong>Offline Mode</strong> before you come to the event. Not doing these things can cause&nbsp; a lot of problems, so please make sure you do it, as we can't guarantee there'll be an internet connection at the event. It may also be worth bringing the latest patches along on a CD or your harddrive, too - just in case.&nbsp; In any case Foshjedi2004 will bring the latest patches for the non-steam games on the forum list on one of his external hard drives. </p>
<p> To connect to the network, you'll need to have your PC setup to use DHCP. Unless you have changed these settings, this is usually the default! Windows calls this something along the lines of "Obtain an IP address automatically" in the TCP/IP properties of your network card. As long as you have a network card, we should be able to help you if you are unsure as to how to configure this.</p>
<h3>Will there be an internet connection?&nbsp;</h3>
<p>There should be a wireless internet connection (the University's uniroam connection, which you need to have registered for to use). It's not entirely guaranteed though. </p>
<h3>I have another question...</h3>
<p>Feel free to e-mail us at games [AT] sucs [DOT] org, or if you're a member of SUCS you can post on our forum: <a href="../forum" title="SUCS Forum">http://sucs.org/forum</a></p>