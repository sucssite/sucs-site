<p><a href="http://www.ipv6.org/">IPv6</a> is the next generation of the Internet Protocol. It is intended to completely replace what is now called IPv4, which is the current version of the Internet Protocol which you will be familiar with.</p>

<p>A major reason for the existence of a new Internet protocol is the impending exhaustion of globally unique IPv4 addresses. Recently the lifetime of IPv4 has been extended through use of network address translation (NAT), and classless inter-domain routing (CIDR - a method of allocating smaller subnets to organisations than the original classful networks).  NAT allows systems without a globally unique address to access the Internet, but causes significant problems for any peer-to-peer applications such as SIP since it effectively turns the Internet into a client-server, rather than peer-to-peer architecture.</p>

<p>IPv6 solves this problem fairly comprehensively by quadrupling the length of IP addresses from 32 bits (in IPv4) to 128 bits. This gives 3.4x10<sup>38</sup> addresses, an extremely huge number indeed. To give you an idea of just how many addresses this is, it allows for approximately 6.67x10<sup>17</sup> addresses per square millimetre, or the equivalent of an IPv6 /48 netblock (2.8x10<sup>14</sup> more addresses than all the IPv4 addresses in existence) per 100 square metres, of the Earth's surface.<br /></p><p>IPv6 also offers other significant improvements over IPv4 such as built in IPSEC VPN support, automatic configuration (plug and play) and improved multicast capabilities.<br /></p>

<h2>What IPv6 looks like</h2>

<p>The currently ubiquitous dotted-decimal notation for IPv4 addresses is rather awkward when dealing with addresses at a bit level, not to mention being long winded. In dotted decimal notation the IPv6 address of <a href="http://www.kame.net">www.kame.net</a> would be 32.1.2.0.0.0.128.2.2.3.71.255.254.165.48.133. In standard IPv6 notation it is:</p>

<p><code>2001:0200:0000:8002:0203:47ff:fea5:3085</code></p>

<p>The leading zeros in any group can be eliminated, thus giving:</p>

<p><code>2001:200:0:8002:203:47ff:fea5:3085</code></p>

<p>In addition, any one series of groups containing only 0 can be omitted:</p>

<p><code>2001:200::8002:203:47ff:fea5:3085</code></p>

<p>Note that this can only be done for one group, since it might otherwise represent either of two numbers.<br /></p><h2>Who can use IPv6?</h2><p>Whilest the preferred method of using IPv6 is via a native IPv6 network, this is usually not possible since most ISPs don't provide IPv6 connectivity.  However, anyone with a global scope IPv4 address and participate in the IPv6 network by using the 6to4 mechanism so long as they don't have to go through a firewall which blocks IPv4 protocol 41 (6-in-4).<br /></p><p>This is very easy to set up on most modern systems.  For example, under Fedora Core you simply need to edit <span style="font-style: italic;">/etc/sysconfig/network</span> and add:<br /></p><div style="margin-left: 40px;"><span style="font-family: courier new,courier,monospace;">NETWORKING_IPV6=yes</span><br style="font-family: courier new,courier,monospace;" /><span style="font-family: courier new,courier,monospace;">IPV6_DEFAULTDEV=tun6to4</span><br style="font-family: courier new,courier,monospace;" /><span style="font-family: courier new,courier,monospace;">IPV6INIT=yes</span><br /></div>And edit your external network interface configuration (e.g. <span style="font-style: italic;">/etc/sysconfig/network-scripts/ifcfg-eth0</span>) and add:<br /><div style="margin-left: 40px;"><span style="font-family: courier new,courier,monospace;">IPV6TO4INIT=yes<br /></span></div>

<p>Please note that if you're using your Linux system as a firewall you will also need to apply firewall rules to the IPv6 network by using <em>ip6tables</em> and if you're routing traffic through the box you'll want to turn on IPv6 forwarding and fiddle with the <em>radvd</em> router advertisement daemon</p>

<h2>Setting up IPv6 on Linux</h2>

<p>If you already have a globally unique IPv4 address, you can use this to access the IPv6 internet using a scheme called 6to4. This involves IPv6 packets being encapsulated within IPv4 packets and forwarded to an IPv6 aware router. You do need an IPv6 aware router to do this; however, you do not need to know its IP address or who it belongs to, as you can get the packets automagically forwarded to the nearest 6to4 gateway using a special <em>anycast</em> address.</p>

<p>First, you need to generate a 6to4 address for your subnet. To find out your current IP address, type:</p>

<p><code>ip addr show</code></p>

<p>Suppose your IPv4 address is 1.2.3.4. If this is globally unique (generally it will be if it isn't in one of the ranges reserved for private networks defined in RFC 1918), you can turn this into a globally unique IPv6 address by simply putting it in IPv6 hexadecimal notation with the prefix 2002::/16, so your Internet-facing address might be 2002:102:304::1. (In fact, 2002:102:304::/48 is your very own subnet which has 2<sup>8</sup> times more addresses than exist in the whole IPv4 address space. If you own a router that has a globally unique IPv4 address you can theoretically set up its DHCP server to hand out globally unique addresses with this prefix to all the hosts it supports.)</p>

<p>Here is a command you can use to calculate this address, given an IPv4 address of 1.2.3.4:</p>

<p><code>printf &quot;2002:%02x%02x:%02x%02x::1&quot; `echo 1.2.3.4 | tr &quot;.&quot; &quot; &quot;`</code></p>

<p>Now you know your IPv6 address, you need to create a 6to4 tunneling pseudodevice. This can be done as root with the <code>ip</code> command like so:</p>

<p><code>ip tunnel add tun6to4 mode sit ttl 30 remote any local 1.2.3.4</code></p>

<p>Note that the 'local' address at the end is your IPv4 address. (Side note: ttl is the default Time To Live header for outgoing packets, meaning the maximum number of routers packets are allowed to go through before they get dropped (this is used just as in IPv4, to stop packets going round in circles if they hit a routing loop). 30 should be plenty. In fact, at this point in time you will often be able to get a connection to another IPv6 host over just one hop, with all of the routers in between using IPv4 and therefore not decrementing the TTL - but just in case, and because this will (hopefully!) not be the case in future, 30 is a good default.)</p>

<p>Now bring this tunnel up (don't worry that you can't use it yet):</p>

<p><code>ip link set dev tun6to4 up</code></p>

<p>Now you need to assign a 6to4 address to the tunnel. This is the address with the prefix 2002::/16 which you worked out earlier.</p>

<p><code>ip -6 addr add 2002:102:304::1/16 dev tun6to4</code></p>

<p>The /16 is important, as it indicates that the 2002 is the 6to4 prefix and not just some arbitrary address. You will still not be able to connect yet, as the network stack doesn't know where to send packets. So you need to set up a default route for Internet traffic. Here is the magic bit. With IPv4 you usually find out the address of your default gateway from DHCP (and in fact you can still do this with IPv6). With 6to4, you can have your upstream router handle IPv6 natively (pretty rare at this point in time), or know the IPv4 address of a 6to4 gateway (which might not be possible or convenient). Or you can send packets out through the IPv4 internet looking for <em>any</em> 6to4 gateway using a special destination address called <em>anycast</em>. This address is 192.88.99.1. Here is how you set it up:</p>

<p><code>ip -6 route add 2000::/3 via ::192.88.99.1 dev tun6to4 metric 1</code></p>

<p>Here the 'metric' argument of 1 indicates that the network stack should prefer to use this route if it can.</p>

<p>Now you should be set up and able to access the IPv6 internet. Try a ping:</p>

<p><code>ping6 www.kame.net</code></p>

<p>If you see responses, you can access the v6 internet. You should also try looking at the <a href="http://www.kame.net/">Kame project website</a> - if you see a dancing turtle at the top, your web browser is using IPv6!</p>