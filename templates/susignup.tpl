{if $mode=='login'}
    <form action="{$componentpath}" method="post">
        <div class="box" style="width: 70%; margin: auto;">
            <div class="boxhead"><h2>Membership Signup</h2></div>
            <div class="boxcontent">

                <p>Please enter your Transation ID. It can be found by logging into <a
                            href='http://www.swansea-union.co.uk/shop/purchasehistory/'>swansea-union.co.uk</a> and
                    selecting purchase history.</p>

                <div class="row">
                    <label for="sid">Student Number:</label>
                    <span class="textinput"><input type="text" size="20" name="sid" id="sid"/></span>
                </div>
                <div class="row">
                    <label for="transactionID">Transaction ID:</label>
                    <span class="textinput"><input type="text" size="20" name="transactionID"
                                                   id="transactionID"/></span>
                </div>
                <div class="row"><span class="textinput">
                <input type="submit" name="submit" value="Join"/></span>
                </div>
                <div class="clear"></div>
                <div class="note">If you already have an account and wish to renew, simply buy an additional years
                    membership from the <a href='https://sucs.org/join'>SUSU website</a>. Enter
                    the details above and click "Join"
                </div>
            </div>
            <div class="hollowfoot">
                <div>
                    <div></div>
                </div>
            </div>
        </div>
    </form>
{elseif $mode=='form'}
    <h1>Signup</h1>
    <p>To contiune signup now click below</p>
    <form action="{$baseurl}/signup/" method="post">
        <input type=hidden name="signupid" id="id" value="{$id}"/>
        <input type=hidden name="signuppw" id="pass" value="{$pass}"/>
        <input type=hidden name="signupsid" id="sid" value="{$sid}"/>
        <input type=submit name="submit" value="Proceed"/>
    </form>
{else}
    <h1>Error</h1>
    <div class='errorbar'>
        <strong>Error: </strong> {$error_text}
    </div>
    An error occured during signup, please email, with as much information as you can provide,
    <a href='mailto:admin@sucs.org'>admin@sucs.org</a>
    for assistance.
{/if}
