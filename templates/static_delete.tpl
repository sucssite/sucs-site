<form class="dialog" action="{$path}" method="post">
    <div class="box">
        <div class="boxhead"><h2>Warning</h2></div>
        <div class="boxcontent">
            <p>Are you sure you want to delete '{$title}'?</p>

            <p><input type="submit" name="action" value="Delete"/>

                <input type="submit" name="action" value="Cancel"/></p>
        </div>
        <div class="hollowfoot">
            <div>
                <div></div>
            </div>
        </div>
    </div>
</form>
