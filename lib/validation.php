<?php
require_once("validationData.php");
require_once("sanitization.php");
/* useful validation functions */

//check for a valid email address
function validEmail($email)
{
    global $error;

    // check for valid syntax
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error = 'Invalid email address (syntax)';
        return false;
    }

    // Syntax OK

    // domain consists of any character after a '@' and cannot contain '@'
    // therefore any character after the last '@' is part of the domain
    $domain = substr($email, strrpos($email, '@') + 1);

    // Check for an mail server
    if (!getmxrr($domain, $mx) or !gethostbyname($domain)) {
        $error = "No mail servers listed for '$domain'";
        return false;
    } else {
        // Email address valid from technical point of view
        return true;
    }
}

# Use cracklib to check for weak passwords.
# returns FALSE if the password is good i.e. not weak
# otherwise returns a string saying why its weak
function weakPassword($password)
{
    // Try fedora then debian known paths
    if (file_exists("/usr/sbin/cracklib-check"))
        $cracklib = "/usr/sbin/cracklib-check";
    else
        if (file_exists("/usr/sbin/crack_testlib"))
            $cracklib = "/usr/sbin/crack_testlib";
        else
            return "Cannot find cracklib";

    $proc = proc_open($cracklib, array(0 => array("pipe", "r"), 1 => array("pipe", "w")), $pipes, '/tmp/', NULL);
    if (!is_resource($proc)) {
        return "Cannot find cracklib";
    }
    fwrite($pipes[0], $password);
    fclose($pipes[0]);
    $last = "";
    do {
        $last = fgets($pipes[1]);
        if ($last !== FALSE) $answer = trim($last);
    } while ($last !== FALSE);
    fclose($pipes[1]);
    proc_close($proc);
    $answer = substr(strrchr($answer, ":"), 2);
    if (strtolower($answer) == "ok") return FALSE;
    if ($answer == "") return ("Empty password");
    return $answer;
}

// check if username is an alias
function isAlias($username)
{
    $ok = false;
    // check its not an alias
    $aliasesfile = file('/etc/aliases');
    foreach ($aliasesfile as $aliasline) {
        if (trim($aliasline) && $aliasline[0] != "#") {
            $anAlias = explode(":", trim($aliasline));
            if ($anAlias[0] && !posix_getpwnam($anAlias[0]) && ($anAlias[0] == $username)) {
                $ok = true;
                return true;
            }
        }
    }
    return $ok;
}

//check if a user with a sid already exists
function sidUsed($sid)
{
    $sucsDB = NewADOConnection('postgres8');
    $sucsDB->Connect('dbname=sucs');
    $sucsDB->SetFetchMode(ADODB_FETCH_ASSOC);
    $query = "SELECT * FROM members WHERE sid=?";
    $data = $sucsDB->GetAll($query, $sid);
    return (sizeof($data) > 0);
}


function validUsername($username)
{
    global $error;
    // check if uname is syntactically valid
    $syntax = preg_match("/^[a-z][a-z0-9_]*$/", $username);
    if (!$syntax || (strlen($username) < 2)) {
        $error = "Usernames must start with a letter, only contain lowercase letters, numbers 0-9 and underscores (_) and be at least 2 characters.";
        return false;
    } // check if the username already exists
    elseif (posix_getpwnam($username)) {
        $error = "Username already taken";
        return false;
    } // check if its a mail alias
    elseif (isAlias($username)) {
        $error = "Username is a mail alias";
        return false;
    } else {
        return true;
    }
}

function validSID($SID, $override)
{
    global $error;
    if ($override) {
        if ($SID == "") {
            $error = "You MUST provide some sort of student number";
            return false;
        } else {
            return true;
        }
    } else {
        if (!preg_match("/^[0-9]{6,}$/", $SID)) {
            $error = "Invalid student ID";
            return false;
        } elseif (sidUsed($SID)) {
            $error = "A user with that student ID already exists, email <a href=\"mailto:admin@sucs.org\">admin@sucs.org</a> if this is an error.";
            return false;
        } elseif (lookupSID($SID) == " ") {
            $error = "Student not found, email <a href=\"mailto:admin@sucs.org\">admin@sucs.org</a> if this is an error.";
            return false;
        } else {
            return true;
        }
    }
}

function validName($realName, $override)
{
    global $error;
    if ($override) {
        if ($realName == "") {
            $error = "You MUST provide some sort of name";
            return false;
        } else {
            return true;
        }
    } else {

        // names can legally be really weird so just check that it is at least 1 visible character
        // followed by any number of non-control characters
        $realName = trim($realName);
        if (!preg_match("/^[[:graph:]][[:print:]]*$/", $realName)) {
            $error = "Invalid name";
            return false;
        } else {
            return true;
        }
    }
}

function validAddress($address)
{
    global $error;
    $address = sanitizeAddress($address);

    // check that they at least entered in something. Address doesn't need to be as strict when the postcode is.
    $regex = "/^.{5,}+$/s";
    if (!preg_match($regex, $address)) {
        $error = "Please supply a valid address.";
        return false;
    } else {
        return true;
    }
}

function validPostcode($postcode)
{
    $postcode = sanitizePostcode($postcode);

    // matches all postcodes following the valid format described in a 2012 government published document
    $postcodeRegex = "/^([A-Z](([0-9][0-9]?)|([A-Z][0-9][0-9]?)|([A-Z]?[0-9][A-Z])) ?[0-9][ABD-HJLNP-UW-Z]{2})$/";
    if (!preg_match($postcodeRegex, $postcode)) {
        return false;
    } else {
        return $postcode;
    }

}

function validPhone($phone)
{
    global $error;
    $phone = sanitizePhone($phone);
    if (!preg_match("/^\+?[0-9-]{10,}$/", $phone)) {
        $error = "Must be all numbers";
        return false;
    }
    return true;
}

function validSignupEmail($email)
{
    global $error;
    if (preg_match('/@sucs\.$/', $email)) {
        $error = "SUCS email addresses are not allowed";
        return false;
    } elseif (!validEmail($email)) {
        return false;
    } else {
        return true;
    }

}

?>
