<?php
function make_password($length = 8)
{
    $vowels = "aeiouy";
    $consonants = "bcdfghjklmnprst";
    $password = "";

    $cn = strlen($consonants) - 1;
    $vn = strlen($vowels) - 1;

    // Start on cons or vowel
    $alt = mt_rand(0, 1);

    // How many numbers
    $len = mt_rand($length - 3, $length);

    for ($i = 0; $i < $len; $i++) {
        if ($alt == 1) {
            $password .= $consonants[mt_rand(0, $cn)];
            $alt = 0;
        } else {
            $password .= $vowels[mt_rand(0, $vn)];
            $alt = 1;
        }
    }
    for ($i = 0; $i < $length - $len; $i++) {
        $password .= mt_rand(0, 9);
    }
    return $password;
}

// https://stackoverflow.com/a/44428794
function cryptPassword($password, $salt = "", $rounds = 5000)
{
        if ($salt == "") {
                // Generate random salt
                $salt = substr(bin2hex(openssl_random_pseudo_bytes(16)),0,16);
        }
        // $6$ specifies SHA512
        $hash = crypt($password, sprintf('$6$rounds=%d$%s$', $rounds, $salt));

        return $hash;
}

function generateUid()
{

    //get the year, this'll be the start/prefix of the uid
    $prefix = date("Y");

    //generate a uid
    //check to see if it's taken/safe to use
    $ok = false;
    while ($ok == false) {

        //generate random number between 00000 and 99999
        $uid = sprintf("%05d", mt_rand(0, 99999));

        //id return 1 for error (safe to take). 0 for success (taken) not safe
        exec("id ".$prefix.$uid, $output, $returnVal);

        //check the result of id
        if ($returnVal == 1) {
            // We have an unused one!
            $ok = true;
            $safeuid = $prefix.$uid;
        }

    }

    return $safeuid;
}

function generateLdif($uid, $password, $type, $realname, $username)
{
    // explode the realname
    $nameexplode = explode(' ', trim($realname));
    // hash the password
    $ldappassword = "{CRYPT}" . cryptPassword($password);
    // compile ldif
    $ldif = "dn: uid=" . $username . ",ou=People,dc=sucs,dc=org\n";
    $ldif .= "uid: " . $username . "\n";
    $ldif .= "cn: " . $realname . "\n";
    // if only has 1 part to real name (and therefore a soc) then set it as sn otherwise set first name to given name and last name to sn
    if (count($nameexplode) > 1) {
        $ldif .= "givenName: " . $nameexplode[0] . "\n";
        $ldif .= "sn: " . $nameexplode[count($nameexplode) - 1] . "\n";
    } else {
        $ldif .= "sn: " . $realname . "\n";
    }
    $ldif .= "mail: " . $username . "@sucs.org\n";
    $ldif .= "objectClass: person\n";
    $ldif .= "objectClass: organizationalPerson\n";
    $ldif .= "objectClass: inetOrgPerson\n";
    $ldif .= "objectClass: posixAccount\n";
    $ldif .= "objectClass: top\n";
    $ldif .= "userPassword: " . $ldappassword . "\n";
    $ldif .= "loginShell: /bin/bash\n";
    $ldif .= "uidNumber: " . $uid . "\n";
    // make some society specific changes
    // More like make sure peoples home dirs get made in the right place
    if ($type == 2) {
        $gid = 1130;
        $homebase = "society";
    } elseif ($type == 5) {
        $gid = 100;
        $homebase = "alumni";
    } elseif ($type == 3) {
        $gid = 100;
        $homebase = "honorary";
    } elseif ($type == 4) {
        $gid = 100;
        $homebase = "life";
    } else {
        $gid = 100;
        $homebase = "member";
    }
    $ldif .= "gidNumber: " . $gid . "\n";
    $ldif .= "homeDirectory: /home/" . $homebase . "/" . $username . "\n";
    $ldif .= "gecos: " . $realname . "\n\n";

    return $ldif;
}

// function to renew a persons sucs membership
function renew_membership($username)
{

    // we need to the sucs db here
    global $sucsDB;

    // get their details from the sucs db
    $userdata = $sucsDB->Execute("SELECT * FROM members WHERE username=?", array($username));

    // include the date file so we can call the paidUntil function
    include_once("date.php");

    // Update their record in the DB
    $sucsDB->Execute("UPDATE members SET paid=?, lastupdate=DEFAULT, lastedit=? WHERE username=?", array(paidUntil(time()), "99999", $username));

    // Give them their 200 print credits
    exec("sudo /usr/local/sbin/printerrenew.apache ${username} 200");

    // apprently sending them an email confirming so is nice
    $message = "Your Swansea University Computer Society (SUCS) membership has been renewed\n\n";
    $message .= "Username: ${username}\n";
    $message .= "If you do not know or have forgotten your password, please email admin@sucs.org to arrange for it to be changed.\n\n";
    $message .= "Regards\n  The SUCS admin";
    $header = "From: admin@sucs.org\r\n";
    $header .= "Reply-To: admin@sucs.org";
    // send it to their personal account
    mail($userdata->fields['email'], "SUCS account renewal", $message, $header);

}

?>
