<?php
// lookup real names from sid's using campus ldap
function lookupSID($sid)
{
    $ds = ldap_connect("192.168.10.16");
    ldap_set_option($ds, LDAP_OPT_NETWORK_TIMEOUT, 10); /* 10 second timeout */
    ldap_bind($ds);
    $sr = ldap_search($ds, "ou=Active,ou=Resources,o=Swansea", "EDUPERSONTARGETEDID=" . $sid);
    $info = ldap_get_entries($ds, $sr);
    ldap_unbind($ds);
    return ucwords(strtolower($info[0]['givenname'][0] . " " . $info[0]['sn'][0]));
}

// lookup addresses from postcodes using the university's website
function lookup_postcode($postcode)
{

    include_once "../paf-key.php";

    $url = "http://paf.sucs.org/?apikey=$apikey&postcode=" . rawurlencode($postcode);

    $req = curl_init($url);
    curl_exec($req);
    curl_close($req);
}

?>
