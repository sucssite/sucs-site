<?php
//ob_start();
//include("../member/signup.php");
//$output = ob_get_contents();
//ob_end_clean();
//

// -------------------------------------------------------------
// DEV: CHANGE THIS FOR DEV MODE
// -------------------------------------------------------------

//$script_path="/home/member/eclipse/signuptests/";
$script_path = "sudo /usr/local/sbin/";

// DEV: CHNAGE THIS FOR DEV MODE
//$error_email="eclipse@sucs.org";
$error_email = "admin@sucs.org";

$override_permission = "sucsstaff";

// is the validation (mostly) overridable
$overridable = isset($session->groups[$override_permission]);

// ------------------------------------------------
// DEV: UNCOMMENT THIS FOR DEV MODE
// ------------------------------------------------

//$overridable=true;

//set defaults
$mode = 'login';

//login
if (isset($_REQUEST['signupid']) && isset($_REQUEST['signuppw'])) {
    //set signup details
    $signupid = $_REQUEST['signupid'];
    $signuppw = $_REQUEST['signuppw'];

    //$sucsDB->debug = true;

    // get row(s)
    $query = "SELECT * FROM signup WHERE id=? AND password=?";
    $array = array($signupid, $signuppw);
    $data = $sucsDB->GetAll($query, $array);
    // if data was returned and it was exactly 1 row
    if (is_array($data) && sizeof($data) == 1) {
        $row = $data[0];
        // if the id hasnt already been used
        if (!(isset($row[activated]) && isset($row[username]))) {
            // pass on the id and passwd and id the validation is overridable
            $smarty->assign("signupid", $signupid);
            $smarty->assign("signuppw", $signuppw);
            // pass on the student id if it exists
            if (isset($_REQUEST['signupsid'])) {
                $smarty->assign("signupsid", $_REQUEST['signupsid']);
            }
            $smarty->assign("overridable", $overridable);
            $smarty->assign("usertype", $row[type]);
            // if accepting the form
            if (isset($_REQUEST['username']) && isset($_REQUEST['realname']) && isset($_REQUEST['email']) && isset($_REQUEST['phone'])) {
                require_once("../lib/validation.php");
                $override = $overridable && (isset($_POST['override']) && $_POST['override'] == "on");
                $valid = true;
                $errors = array();
                $fields = array();
                if (!validUsername($_REQUEST['username'])) {
                    $valid = false;
                    $errors['username'] = $error;
                }
                $fields['username'] = $_REQUEST['username'];
                if (!(validSignupEmail($_REQUEST['email']) || $override)) {
                    $valid = false;
                    $errors['email'] = $error;
                }
                $fields['email'] = $_REQUEST['email'];
                if (!(validPhone($_REQUEST['phone']) || $override)) {
                    $valid = false;
                    $errors['phone'] = $error;
                }
                $fields['phone'] = sanitizePhone($_REQUEST['phone']);
                if ($row[type] != 2) {
                    if (!(validAddress($_REQUEST['address']) || $override)) {
                        $valid = false;
                        $errors['address'] = $error;
                    }
                    $fields['address'] = sanitizeAddress($_POST['address']);
                    if (!validName($_REQUEST['realname'], $override)) {
                        $valid = false;
                        $errors['realname'] = $error;
                    }
                    $fields['realname'] = $_REQUEST['realname'];
                } else {
                    if (!(validName($_REQUEST['contact'], false) || $override)) {
                        $valid = false;
                        $errors['contact'] = $error;
                    }
                    $fields['contact'] = $_REQUEST['contact'];
                    if (!validName($_REQUEST['realname'], $override)) {
                        $valid = false;
                        $errors['realname'] = $error;
                    }
                    $fields['realname'] = $_REQUEST['realname'];
                }
                if ($row[type] == 1) {
                    if (!validSID($_REQUEST['studentid'], $override)) {
                        $valid = false;
                        $errors['studentid'] = $error;
                    }
                    $fields['studentid'] = $_REQUEST['studentid'];
                }

                if ($valid) {
                    // include membership adding functions
                    require_once("../lib/member_functions.php");
                    $mode = 'result';
                    $failed = false;

                    // invalidate signup slip
                    $query = "UPDATE signup SET sid=?, username=?, activated=now() WHERE id=?";

                    unset($atribs);
                    $atribs[0] = $fields['studentid'];
                    $atribs[1] = $fields['username'];
                    $atribs[2] = $signupid;
                    $responce = $sucsDB->Execute($query, $atribs);
                    if (!$responce) {
                        mail(
                            $error_email,
                            "Signup Error",
                            "Unable to invalidate signup slip: " . $signupid . "\nAborting\nError message:\n" . $sucsDB->ErrorMsg(),
                            "From: \"SUCS Admin\" <admin@sucs.org>"
                        );
                        $failed = true;
                    } else {
                        //generate the new uid
                        $uid = generateUid();
                        // make a password
                        $password = make_password();
                        // make the ldif
                        $ldif = generateLdif($uid, $password, $row[type], $fields['realname'], $fields['username']);
                        // write ldif file
                        file_put_contents('/tmp/useradd.' . $fields['username'] . '.ldif', $ldif);
                        exec(
                            $script_path . 'useradd.apache ' .
                            escapeshellarg($fields['username']) . ' ' .
                            escapeshellarg($fields['studentid']) . ' ' .
                            escapeshellarg($fields['email']) .
                            ' 2>&1',
                            $execoutputarr,
                            $execreturn
                        );
                        if ($execreturn != 0) {
                            $execoutputstr = implode("\n", $execoutputarr);
                            mail(
                                $error_email,
                                "Error creating user for signup id: " . $signupid,
                                $execoutputstr,
                                "From: \"SUCS Admin\" <admin@sucs.org>"
                            );
                            $failed = true;
                        }
                        // DEV: STICK THIS FOR DEV
                        if (posix_getpwnam($fields['username'])) {
                            $query = "SELECT count(*) from members where username = ?";
                            $data = $sucsDB->GetAll($query, array($fields['username']));
                            if ($data[0]['count'] != 0) {
                                mail(
                                    $error_email,
                                    "Signup Error",
                                    "User " . $fields['username'] . " already exists in the database, THIS SHOULD NEVER HAPPEN\n Love, the signup system.\n\nP.S. the signup id is: " . $signupid,
                                    "From: \"SUCS Admin\" <admin@sucs.org>"
                                );
                                $failed = true;
                            } else {
                                $query = "INSERT INTO members (";
                                $query .= "uid, username, realname, email, address, phone, sid, type, paid, lastedit, comments";
                                $query .= ") values (";
                                $query .= "?, ?, ?, ?";
                                // if its a soc then it has no address
                                if ($row[type] != 2) {
                                    $query .= ", ?"; //address
                                } else {
                                    $query .= ", DEFAULT"; //address
                                }
                                $query .= ", ?"; //phone
                                // only student had a sid
                                if ($row[type] == 1) {
                                    $query .= ", ?"; //sid
                                } else {
                                    $query .= ", DEFAULT";
                                }
                                $query .= ", ?, ?, ?";
                                // socienty add a default comment
                                if ($row[type] == 2) {
                                    $query .= ", ?";// comment
                                } else {
                                    $query .= ", DEFAULT";
                                }
                                $query .= ");";
                                unset($atribs);
                                $atribs[] = $uid;
                                $atribs[] = $fields['username'];
                                $atribs[] = $fields['realname'];
                                $atribs[] = $fields['email'];
                                if ($row[type] != 2) {
                                    $atribs[] = $fields['address'];
                                }
                                $atribs[] = $fields['phone'];
                                if ($row[type] == 1) {
                                    $atribs[] = $fields['studentid'];
                                }
                                $atribs[] = $row[type];
                                include_once("../lib/date.php");
                                $atribs[] = paidUntil(time());
                                $atribs[] = $uid;
                                if ($row[type] == 2) {
                                    $atribs[] = "Contact name: " . $fields['contact'];
                                }
                                $responce = $sucsDB->Execute($query, $atribs);
                                // if somthing broke then email
                                if (!$responce) {
                                    mail(
                                        $error_email,
                                        "Signup Error",
                                        "Database problems for signup id: " . $signupid . "\nError message:\n" . $sucsDB->ErrorMsg(),
                                        "From: \"SUCS Admin\" <admin@sucs.org>"
                                    );
                                    $failed = true;
                                } else {
                                    // if door card is connected to the signup slip move it to its proper home
                                    if ($row[card] != '') {
                                        $query = "INSERT INTO doorcards (uid,cardnumber) VALUES (?, ?);";
                                        unset($atribs);
                                        $atribs[0] = $uid;
                                        $atribs[1] = $row[card];
                                        $responce = $sucsDB->Execute($query, $atribs);
                                        if (!$responce) {
                                            mail(
                                                $error_email,
                                                "Signup Error",
                                                "Failed to migrate card details for signup id: " . $signupid . "\nError message:\n" . $sucsDB->ErrorMsg(),
                                                "From: \"SUCS Admin\" <admin@sucs.org>"
                                            );
                                        }
                                    }

                                    $logsmessage = "New user '" . $fields['username'] . "' has been created on SUCS\n";
                                    $logsmessage .= "at: " . date("H:i ", time()) . " on " . date("l F jS Y", time()) . "\n";
                                    $logsmessage .= "From: " . $_SERVER['REMOTE_ADDR'] . "\n";
                                    $logsmessage .= "Using signup id: " . $signupid . "\n";
                                    if ($override) {
                                        $logsmessage .= "User " . $session->username . " overrode validation.\n";
                                    }
                                    $logsmessage .= "Love, The Signup System";
                                    // DEV: CHANGE THIS EMAIL ADDRESS
                                    mail(
                                        "logs@sucs.org,treasurer@sucs.org",
                                        "User '" . $fields['username'] . "' Created on SUCS",
                                        $logsmessage,
                                        "From: \"SUCS Admin\" <admin@sucs.org>"
                                    );

                                    $usermessage = "Welcome to the Swansea University Computer Society!\n\n";
                                    $usermessage .= "Your account details are:\n\n";
                                    $usermessage .= "Username: " . $fields['username'] . "\n";
                                    $usermessage .= "Password: " . $password . "\n\n";
                                    $usermessage .= "Wondering what to do next? Check out our Getting Started page: http://sucs.org/Getting%20Started or go right ahead and post on our forum at http://sucs.org/Community/Forum or join in the discussion on our chat system, Milliways: http://sucs.org/Community/Milliways\n\n";
                                    $usermessage .= "Our weekly social is held in the coffee end JC's at 1:00 PM every Wednesday\n\n";
                                    $usermessage .= "Before you use the SUCS computers or the computer room, please make sure you are familiar with the conditions of use and room rules at http://sucs.org/About/Regulations\n\n";
                                    $usermessage .= "If you require help using the system, introductory guides are available at http://sucs.org/Knowledge\n\n";
                                    $usermessage .= "If you have any trouble using the system, reply to this e-mail describing the nature of the problem and we'll look into it.\n\n";
                                    $usermessage .= "We hope you enjoy your SUCS membership.\n\n";
                                    $usermessage .= "Regards,\n\n";
                                    $usermessage .= "Swansea University Computer Society";
                                    if ($fields['email'] != '') {
                                        $user_email = $fields['email'];
                                    } elseif ($fields['studentid'] != '') {
                                        $user_email = $fields['studentid'] . "@swan.ac.uk";
                                    } else {
                                        $user_email = FALSE;
                                    }
                                    if ($user_email) {
                                        mail(
                                            $user_email,
                                            "Your SUCS Account has been created!",
                                            $usermessage,
                                            "From: \"SUCS Admin\" <admin@sucs.org>"
                                        );
                                    }
                                }
                            }
                        }

                        //Wrong logic, only students have student email addresses ~imranh
                        if ($row['type'] == 1) {
                            $addtolist = "" . $fields['email'] . "\n" . $fields['studentid'] . "@swan.ac.uk";
                        } else {
                            $addtolist = "" . $fields['email'] . "\n"; //Societies don't have student email addresses
                        }
                        file_put_contents('/tmp/listadd.' . $fields['username'], $addtolist);
                        unset($execoutputarr);
                        exec(
                            $script_path . 'listadd.apache ' .
                            escapeshellarg($fields['username']) . ' ' .
                            escapeshellarg($row[type]) .
                            ' 2>&1',
                            $execoutputarr,
                            $execreturn
                        );
                        if ($execreturn != 0) {
                            $execoutputstr = implode("\n", $execoutputarr);

                            mail(
                                $error_email,
                                "Error adding user to mailing lists from signup id: " . $signupid,
                                $execoutputstr . $execreturn,
                                "From: \"SUCS Admin\" <admin@sucs.org>"
                            );
                            $failed = true;
                        }
                        exec($script_path . 'printeradd.apache ' . escapeshellarg($fields['username']));
                    }
                    $smarty->assign("failed", $failed);
                    if ($failed) {
                        $errorreparray = $_POST;
                        unset($errorroparray[signuppw]);
                        $errorreparray[uid] = $uid;
                        $errorreport = "User input details:\n\n";
                        foreach ($errorreparray as $key => $value) {
                            $errorreport .= $key . ": " . $value . "\n";
                        }
                        $errorreport .= "\nLove, the Signup System";
                        mail(
                            $error_email,
                            "Signup system error report",
                            $errorreport,
                            "From: \"SUCS Admin\" <admin@sucs.org>"
                        );
                    } else {
                        $smarty->assign("username", $fields['username']);
                        $smarty->assign("password", $password);
                        $smarty->assign("email", $user_email);
                    }

                } else {
                    //re-show form
                    $script = "<script language='javascript' type='text/javascript' src='" . $baseurl . "/js/jquery.js'></script>\n";
                    $script .= "<script language='javascript' type='text/javascript' src='$baseurl/js/signup.js'></script>\n";
                    $smarty->assign("fields", $fields);
                    $smarty->assign("errors", $errors);
                    $smarty->append('extra_scripts', $script);
                    $mode = 're-form';
                }
            } else {
                // display the form
                $script = "<script language='javascript' type='text/javascript' src='" . $baseurl . "/js/jquery.js'></script>\n";
                $script .= "<script language='javascript' type='text/javascript' src='$baseurl/js/signup.js'></script>\n";
                $smarty->append('extra_scripts', $script);
                $mode = 'form';
            }
        } else trigger_error("Signup ID already used", E_USER_WARNING);
    } else trigger_error("Invalid ID or Password", E_USER_WARNING);

}
//Set smarty Variables
$smarty->assign("mode", $mode);
$output = $smarty->fetch("signup.tpl");

$smarty->assign("title", "Sign Up");
$smarty->assign("body", $output);

?>
