<?php

include("../lib/members.php");
include_once("../lib/date.php");
$members = new Members;

if ($session->loggedin) {
    $usernames = $members->getMemberList();

    $smarty->assign('members', $usernames);
    $sidebar = $smarty->fetch('members-search.tpl');
    $smarty->assign('secondary', $sidebar);

    // Show Searched for users depending on the method provided
    $value = '';
    $uid = false;
    $pathCount = count($pathlist) - 1;
    if (isset($_POST['search'])) {
        $value = $_POST['search'];
        $uid = true;
        $usernames = $members->memberSearch($value);
    } else if (isset($_POST['member'])) {
        $value = $_POST['member'];
        $uid = true;
        $usernames = $members->memberView($value);
    } else if ($component['path'] != $path) {
        $value = $pathlist[$pathCount];
        $usernames = $members->memberView($value);
    }
    if (!empty($value)) {
        // Redirect if we have found a valid single user
        if (count($usernames) == 1) {
            $smarty->assign('who', $usernames[0]['uid']);
            // Add banana widget to the sidebar
			$who = $usernames[0]['uid'];
			//Tell the banana library that we're dealing with a real member
			$isnonmember = false;
            include("../lib/banana-admin.php");
            // Needs Redirection
            if ($uid) {
                header('Location: ' . $component['path'] . '/' . $usernames[0]['uid']);
            } else {
                // Pictures
                if (file_exists('pictures/people/' . $usernames[0]['uid'] . '.png')) {
                    $usernames[0]['picture'] = '/pictures/people/' . $usernames[0]['uid'] . '.png';
                }
                // Account type
                $homedir = explode('/', $usernames[0]['homedir']);
                $usernames[0]['acctype'] = ucfirst($homedir[2]);
                // Project
                if (file_exists($usernames[0]['homedir'] . '/.project') && ! is_link($usernames[0]['homedir'] . '/.project')) {
                    $usernames[0]['project'] = file_get_contents($usernames[0]['homedir'] . '/.project');
                }
                // Plan
                if (file_exists($usernames[0]['homedir'] . '/.plan') && ! is_link($usernames[0]['homedir'] . '/.plan')) {
                    $usernames[0]['plan'] = file_get_contents($usernames[0]['homedir'] . '/.plan');
                }
                // Website
                if (file_exists($usernames[0]['homedir'] . '/public_html')) {
                    $usernames[0]['website'] = true;
                }
                // Bananas stuff
                $bananasql = "SELECT *, date_trunc('second', whn) FROM awards ";
                $bananasql .= "WHERE username ~* ? ORDER BY WHN DESC";
                $awards = $DB->GetAll($bananasql, array("^" . $usernames[0]['uid'] . "$"));

                // arrange by academic year, calculate sums as we go
                foreach ($awards as $award) {
                    $acyear = academicYear(strtotime($award['whn']));
                    $awards_by_year[$acyear]['awards'][] = $award;
                    $awards_by_year[$acyear]['sum'] += $award['score'];
                    $bananasum += $award['score'];
                }

                $usernames[0]['awardsbyyear'] = &$awards_by_year;
                $usernames[0]['bananascore'] = $bananasum;
            }
        }
        $smarty->assign('results', $usernames);

    } else {
        // generate fun data to put on index page

        // top 5
        $stats['top'] = $DB->GetAll("SELECT username, sum(score) as sum FROM awards GROUP BY username ORDER BY sum DESC LIMIT 5");

        // top 5 (this academic year)
        $yeartop_sql = "SELECT username, sum(score) as sum FROM awards ";
        $yeartop_sql .= "WHERE whn > date(?) GROUP BY username ORDER BY sum DESC LIMIT 5";
        $stats['yeartop'] = $DB->GetAll($yeartop_sql, array(academicYear(time()) . "-09-01"));

        // bottom 5
        // exclude sambws and frostys allways decresing banana that scres up the result
        $stats['bottom'] = $DB->GetAll("SELECT username, sum(score) as sum FROM awards WHERE id!=685 and id!=393 GROUP BY username ORDER BY sum ASC LIMIT 5");
        // recent awards
        // today's definition of "recent" is all awards within three days of the newest award and a minimum of five...
        // if this seems like a dumb method then try something else :-)
        $recentAwards_sql = "(SELECT * FROM awards WHERE whn > (SELECT whn - interval '3 days' FROM awards ORDER BY whn DESC LIMIT 1))";
        $recentAwards_sql .= " UNION (SELECT * FROM awards ORDER BY whn DESC LIMIT 5) ORDER BY 4 DESC";
        $stats['recent'] = $DB->GetAll($recentAwards_sql);


        // retrieve the usernames for top stats, bottom stats, and all users - so we can filter out who's a user and who's not
        foreach ($usernames as $i => $value) {
            $realUsers[] = $usernames[$i]['uid'];
        }
        foreach ($stats['yeartop'] as $i => $value) {
            if (in_array(strtolower($stats['yeartop'][$i]['username']), $realUsers)) {
                $stats['yeartop'][$i]['real'] = TRUE;
            } else {
                $stats['yeartop'][$i]['real'] = FALSE;
            }
        }
        foreach ($stats['top'] as $i => $value) {
            if (in_array(strtolower($stats['top'][$i]['username']), $realUsers)) {
                $stats['top'][$i]['real'] = TRUE;
            } else {
                $stats['top'][$i]['real'] = FALSE;
            }
        }
        foreach ($stats['bottom'] as $i => $value) {
            if (in_array(strtolower($stats['bottom'][$i]['username']), $realUsers)) {
                $stats['bottom'][$i]['real'] = TRUE;
            } else {
                $stats['bottom'][$i]['real'] = FALSE;
            }
        }
        foreach ($stats['recent'] as $i => $value) {
            if (in_array(strtolower($stats['recent'][$i]['username']), $realUsers)) {
                $stats['recent'][$i]['real'] = TRUE;
            } else {
                $stats['recent'][$i]['real'] = FALSE;
            }
        }

        $smarty->assign("stats", $stats);
        $secondary = $smarty->getTemplateVars("secondary");
        $secondary .= $smarty->fetch("banana-leaders.tpl");
        $smarty->assign("secondary", $secondary);

    }
} else {
    // not logged in. Show a list of members with websites
    $usernames = $members->getMemberList();

    foreach ($usernames as $user) {
        if (file_exists($user['homedir'] . '/public_html')) {
            $public_usernames[] = $user;
        }
    }
    $smarty->assign("public_members", $public_usernames);
}

$smarty->assign('url', $component['path']);
$smarty->assign('extra_styles', "/css/members.css");
$result = $smarty->fetch('members.tpl');
$smarty->assign('title', "Members");
$smarty->assign('body', $result);

// End Actual Code
?>
