<?php exec("/usr/local/bin/shame.py -a", $Shame);
$max = 10; //SET to number of users to display
$smarty->assign('title', "The SUCS Hall of Shame");
//$o="<div style='text-align: center;'><img src=\"/images/disk.graph.php\" alt=\"pie chart of top 12 disk users\"/></div>";
$o .= "<table align=\"center\">\n";
$o .= "\t<tr><th align=\"center\">User</th><th align=\"center\">Home Directory Size</th></tr>\n";
for ($i = 1; $i < 13 /*(count($Shame) - 1)*/; $i++) {
    list($space, $place, $user, $size) = preg_split("/ +/", $Shame[$i]);
    if ($size > 0)
        $o .= "\t<tr><td align=\"center\">$user</td><td align=\"center\">${size}B</td></tr>\n";
}
exec("df -m /home", $dfraw);
$dfout = preg_split('/[ ]+/', $dfraw[1]);
$o .= "\t<tr><td align=\"center\" colspan=\"2\">" . floor($dfout[2] / 1024) . "GB used of " . floor($dfout[1] / 1024) . "GB (" . floor($dfout[3] / 1024) . "GB available)</td></tr>\n";
$o .= "</table>\n";
$o .= $Shame[count($Shame) - 1];
$o .= "<br />\n";
//readfile("tail.html");
$out = "<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>
		    <script type=\"text/javascript\">
      			google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});
			google.setOnLoadCallback(drawChart);
			function drawChart() {

		        	var data = new google.visualization.DataTable();
			        data.addColumn('string', 'User');
			        data.addColumn('number', 'Megabytes in use');
				data.addRows(" . (count($Shame) - 1) . ");\n";
for ($i = 0; $i < (count($Shame) - 3); $i++) {
    list($u, $d) = parseLine($Shame[$i + 1]);
    $out .= "data.setValue(" . ($i + 1) . ", 0, '" . $u . "');\n";
    $out .= "data.setValue(" . ($i + 1) . ", 1," . $d . ");\n";
}
$others = 0;
for ($i = $max; $i < count($data); $i++) {
    list($u, $d) = parseLine($Shame[$i + 1]);
    $others += $d;
}
$out .= "data.setValue(0, 0, 'Free Space');\ndata.setValue(0,1," . $dfout[3] . " );\n";
$out .= "\t\tvar chart = new google.visualization.PieChart(document.getElementById('chart_div'));
	           chart.draw(data, {width: 500, height: 360, backgroundColor: 'none', is3D: true, title: 'Disk Usage',sliceVisibilityThreshold: 1/180});
	           }
	</script>";
$out .= "<div id='chart_div' align='center'></div>";
$smarty->assign('body', ($out . $o));
function parseLine($l)
{
    $fields = explode("|", preg_filter("/[ ]+/", "|", $l));
    return array(trim($fields[2]), trim(str_replace("M", "", $fields[3])));
}

?>
