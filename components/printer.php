<?PHP
/******
 * Printer Shame List
 * Ported from old site by tswsl1989
 *
 */
$max = 10; //Set this to number of users to display in chart

$timestamp = '2014-02-05 18:29:02+00'; //display printer stats since this time

$smarty->assign('title', "Printer Statistics");

// $out="<img src=\"/images/printer.graph.php\" alt=\"pie chart of top 12 printer users\"/>";
$table = "<table border=1 cellpadding=3>\n<tr>\n<th>Username</th>\n<th>Pages</th>\n<th>Projected Monthly Usage</th>\n<th>Printed This Month</th>\n<th>What it would cost in the library</th></tr>\n";

//pull in all the current users (ie not old) who have printed stuff
$res = $sucsDB->Execute("select username, sum(pages) as pages from printer where username != 'old' AND start > '" . $timestamp . "' group by username order by sum(pages) desc");
$num = $res->RecordCount();
//for each row stuff the infomation into an array, then free up the database resorces
for ($i = 0; $i < $num; $i++) {
    $data[$i] = $res->FetchRow();
    $data[$i]['month'] = 0;
}
$res->Close();

//get the second since epoch
$thisSecond = time();
//for each user who has printed stuff add the first printed time (since epoch) to the array 
for ($i = 0; $i < $num; $i++) {
    $res = $sucsDB->Execute("select date_part('epoch',start) as first from printer where username ='" . $data[$i]['username'] . "' order by start asc limit 1;");
    $temp = $res->FetchRow();
    $data[$i]['first'] = $temp['first'];
    $res->Close();
}

$res = $sucsDB->Execute("select username, pages, start from printer where username != 'old'");
$datesnum = $res->RecordCount();//for each row stuff the infomation into an array, then free up the database resorces
for ($i = 0; $i < $datesnum; $i++) {
    $dates[$i] = $res->FetchRow($res, $i);
}
$res->Close();


// get the current month
$theMonth = date("m");
$theYear = date("Y");
for ($i = 0; $i < $datesnum; $i++) {
    $userMonth = substr($dates[$i]['start'], 5, 2);
    $userYear = substr($dates[$i]['start'], 0, 4);
    if (($userMonth == $theMonth) && ($userYear == $theYear)) {
        for ($j = 0; $j < $num; $j++) {
            if ($data[$j]['username'] == $dates[$i]['username']) {
                $data[$j]['month'] += $dates[$i]['pages'];
            }
        }
    }
}

//start the total counter
$total = 0;
//for each user.. print a row in the table
for ($i = 0; $i < $num; $i++) {
    $table .= "<tr align=\"center\">\n";
    $table .= "<td>{$data[$i]['username']}</td>\n";
    $table .= "<td>{$data[$i]['pages']}</td>\n";
    //avg pages per month = seconds in a month / (now - first print) * pages
    $crazyGuess = round((2629743.83 / ($thisSecond - $data[$i]['first'])) * $data[$i]['pages']);
    //if the user has only started printing in the last month its not sensible to give a guess of average useage
    if ($thisSecond - $data[$i]['first'] <= 2629743.83) {
        $table .= "<td>Not enough data</td>\n";
    } //traffic lights for pinter useage, adjust values as required
    elseif ($crazyGuess <= 25) {
        $table .= "<td BGCOLOR=\"Green\">" . $crazyGuess . "</td>\n";
    } elseif ($crazyGuess <= 33) {
        $table .= "<td BGCOLOR=\"Orange\">" . $crazyGuess . "</td>\n";
    } else {
        $table .= "<td BGCOLOR=\"Red\">" . $crazyGuess . "</td>\n";
    }
    if ($data[$i]['month'] >= 300) {
        $table .= "<td BGCOLOR=\"Red\">{$data[$i]['month']}</td>\n";
    } else {
        $table .= "<td>{$data[$i]['month']}</td>\n";
    }
    $table .= "<td>" . sprintf("£ %01.2f", round(0.05 * $data[$i]['pages'], 2)) . "</td>\n";
    //add this users useage to the total
    $total += $data[$i]['pages'];
    $table .= "</tr>\n";
}

//get the first print date
$date['first'] = getdate("1391624942");

//get the last print date
$res = $sucsDB->Execute("select date_part('epoch',start) as last from printer order by start desc limit 1;");
$temp = $res->FetchRow();
$date['last'] = getdate($temp['last']);
$res->Close();
//get the number of pages printed by "old" users and add it to the total
$res = $sucsDB->Execute("select sum(pages) as pages from printer where username = 'old' group by username order by sum(pages) desc");
$old = $res->FetchRow();
$old = $old['pages'];
$res->Close();
$total += $old;
//Output two lines at the bottem of the table, one for "old" users and the other for the total
$table .= "<tr align=\"center\">\n\t<th>Old Users</th>\n\t<th>$old</th>\n\t<th>n/a</th>\n</tr>";
$table .= "<tr align=\"center\">\n\t<th>Total</th>\n\t<th>$total</th>\n\t<th>" . round((2629743.83 / ($thisSecond - $date['first'][0])) * $total) . "</th>\n</tr>";
$table .= "</table>\n";

//output the dates of the first and last prints recorded
$out .= "<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>
    <script type=\"text/javascript\">
      google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'User');
	data.addColumn('number', 'Pages Printed');
        data.addRows(" . ($max + 2) . ");\n";
for ($i = 0; $i < ($max + 1); $i++) {
    $out .= "data.setValue(" . ($i) . ", 0, '" . $data[$i]['username'] . "');\n";
    $out .= "data.setValue(" . ($i) . ", 1," . $data[$i]['pages'] . ");\n";
}
$others = 0;
for ($i = $max; $i < count($data); $i++) {
    $others += $data[$i]['pages'];
}
$out .= "data.setValue(" . ($max + 1) . ", 0, 'Others');\ndata.setValue(" . ($max + 1) . ",1, $others);\n";
$out .= "\t\tvar chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, {width: 500, height: 360, backgroundColor: 'none', is3D: true, title: 'Printer Usage'});
      }
    </script>";
$out .= "<div id=\"chart_div\" style=\"background: none;\"></div>";
$out .= "<p>First Data -  " . $date['first'][weekday] . ", " . $date['first'][mday] . " " . $date['first'][month] . " " . $date['first'][year] . ", at " . sprintf("%02d:%02d:%02d", $date['first'][hours], $date['first'][minutes], $date['first'][seconds]) . "<br />";
$out .= "Last Data -  " . $date['last'][weekday] . ", " . $date['last'][mday] . " " . $date['last'][month] . " " . $date['last'][year] . ", at " . sprintf("%02d:%02d:%02d", $date['last'][hours], $date['last'][minutes], $date['last'][seconds]) . "</p>";
//do some silly maths to work out lots of silly things
$paperarea = round(0.21 * 0.297 * $total, 3); //area of paper
$paperweight = round($paperarea * .08, 3); //how much that would weigh
$numtrees = round($paperweight / 730.296, 6); //*very* roughly how many trees that would be
$cost = sprintf("£%01.2f", round(0.05 * $total, 2));
$out .= "<p>That's " . $paperarea . "m<sup>2</sup> of paper, weighing " . $paperweight . "kg!<br>\n";
$out .= "This is equivalent to approximately " . $numtrees . " trees.<br>\n";
$out .= "That would have cost our members a grand total of " . $cost . " if it were printed in the library.. not bad for £5 each!";

//done
$out .= "<p>Note: The number of pages is the number spooled and may be more than the actual number printed</p>";
$out .= $table;
$sucsDB->Close();
$smarty->assign('body', $out);
?>
