<?php

function smarty_function_articlesummary($params, &$smarty)
{

    $article = $params['article'];
    $id = $params['id'];
    $title = rawurlencode($params['title']);

    $linky = "<span style=\"float: right\"><a href=\"/News/" . rawurlencode($title) . "\">Read More</a></span>";

    $matches = array();
    preg_match("/^(<p>.*?<\/p>)/s", $article, $matches);
    $summary = preg_replace("/<\/p>/", "$linky</p>", $matches[0]);
    return $summary;
}

?>
